//
//  Game+CoreDataProperties.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import Foundation
import CoreData

extension Game {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Game> {
        return NSFetchRequest<Game>(entityName: "Game");
    }

    @NSManaged public var awayTeam: String?
    @NSManaged public var awayTeamScore: Int16
    @NSManaged public var homeTeam: String?
    @NSManaged public var homeTeamScore: Int16
    @NSManaged public var identifier: String?
    @NSManaged public var inning: Int16
    @NSManaged public var inningState: String?
    @NSManaged public var status: String?
    @NSManaged public var time: String?
    @NSManaged public var time_zone: String?
    @NSManaged public var venue: String?
    @NSManaged public var dayOfYear: Int16
    @NSManaged public var statusGroup: Int16

}
