//
//  GameTableViewCell.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var homeTeamScoreLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var awayTeamScoreLabel: UILabel!
    @IBOutlet weak var gameStatusLabel: UILabel!

    func configureCell(game: Game) {
     
        awayTeamLabel.text = game.awayTeam
        awayTeamScoreLabel.text = String(game.awayTeamScore)
        homeTeamLabel.text = game.homeTeam
        homeTeamScoreLabel.text = String(game.homeTeamScore)
        gameStatusLabel.text = game.gameListStatus()
        
        setBackgroundColor(game: game)
    }
    
    func setBackgroundColor(game: Game) {
        
        switch game.statusGroup {
        case GameStatusGroup.InProgress.rawValue:
            self.backgroundColor = UIColor.green
        case GameStatusGroup.Pregame.rawValue:
            self.backgroundColor = UIColor.blue
        case GameStatusGroup.Postgame.rawValue:
            self.backgroundColor = UIColor.lightGray
        default:
            self.backgroundColor = UIColor.white
        }
    }
}
