//
//  UpdateCoreDataOperation.swift
//  MLBScores
//
//  Created by Nick Harris on 8/21/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit
import CoreData

class UpdateCoreDataOperation: Operation {

    let persistentContainer: NSPersistentContainer
    var allGameData: [GameData]?
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
    
    override func main() {
        
        guard let allGameData = allGameData else {
            print("No game data available")
            return
        }
        
        let backgroundContext = persistentContainer.newBackgroundContext()
        
        backgroundContext.performAndWait {
            [weak self] in
            
            self?.updateGameData(allGameData: allGameData, backgroundContext: backgroundContext)
            self?.deleteOldGameData(backgroundContext: backgroundContext)
            
            do {
                print("Save Core Data")
                try backgroundContext.save()
            }
            catch let error as NSError {
                print("Error saving CoreData: \(error.localizedDescription)")
            }
        }
    }
    
    func updateGameData(allGameData: [GameData], backgroundContext: NSManagedObjectContext) {
        
        for gameData in allGameData {
            
            var gameDataObject = fetchGameCoreDataObject(identifier: gameData.identifier, backgroundContext: backgroundContext)
            setGameDataObjectProperties(gameData: gameData, gameCoreDataObject: &gameDataObject)
        }
    }
    
    func fetchGameCoreDataObject(identifier: String, backgroundContext: NSManagedObjectContext) -> Game {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Game.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        
        let game: Game
        do {
            let fetchResults: NSArray = try backgroundContext.fetch(fetchRequest)
            
            guard fetchResults.count <= 1 else {
                fatalError("Multile games returned in Core Data fetch")
            }
            
            if fetchResults.count == 0 {
                print("New Game Core Data")
                let entityDescription = NSEntityDescription.entity(forEntityName: "Game", in: backgroundContext)
                game = NSManagedObject.init(entity: entityDescription!, insertInto: backgroundContext) as! Game
            }
            else {
                print("Update Game Core Data")
                game = fetchResults[0] as! Game
            }
        }
        catch let error {
            fatalError(error.localizedDescription)
        }
        
        return game;
    }
    
    func setGameDataObjectProperties(gameData: GameData, gameCoreDataObject: inout Game) {
        
        gameCoreDataObject.awayTeam = gameData.awayTeam
        gameCoreDataObject.awayTeamScore = gameData.awayTeamScore
        gameCoreDataObject.dayOfYear = Date.todaysDayOfYear()
        gameCoreDataObject.homeTeam = gameData.homeTeam
        gameCoreDataObject.homeTeamScore = gameData.homeTeamScore
        gameCoreDataObject.identifier = gameData.identifier
        gameCoreDataObject.inning = gameData.inning
        gameCoreDataObject.inningState = gameData.inningState
        gameCoreDataObject.status = gameData.status
        gameCoreDataObject.time = gameData.time
        gameCoreDataObject.time_zone = gameData.time_zone
        gameCoreDataObject.venue = gameData.venue
        gameCoreDataObject.statusGroup = gameData.statusGroup.rawValue
    }
    
    func deleteOldGameData(backgroundContext: NSManagedObjectContext) {
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Game.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "dayOfYear != %d", Date.todaysDayOfYear())
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            print("Delete Old Core Data")
            try backgroundContext.execute(deleteRequest)
        }
        catch let error as NSError {
            print("Error deleting from CoreData: \(error.localizedDescription)")
        }
    }
}
