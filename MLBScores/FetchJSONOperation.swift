//
//  FetchJSONOperation.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit

class FetchJsonOperation: Operation {
    
    let urlString: String
    var fetchedJson: NSDictionary?
    
    init(urlString: String) {
        
        self.urlString = urlString
        self.fetchedJson = nil
        
        override_executing = false
        override_finished = false
    }
    
    override func main() {
        
        if let jsonURL = URL(string: urlString) {
            
            let session = URLSession.shared
            var request = URLRequest(url: jsonURL)
            
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            let fetchJsonTask = session.dataTask(with: request as URLRequest, completionHandler: {
                [unowned self]
                data, response, error -> Void in
                
                if let error = error {
                    
                    print(error.localizedDescription)
                    self.completeOperation()
                    return
                }
                
                if let data = data {
                    self.createJsonDictionary(data: data)
                }
                else {
                    print("No data returned")
                }
                
                self.completeOperation()
                })
            
            fetchJsonTask.resume()
        }
    }
    
    func createJsonDictionary(data: NSData) {
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? NSDictionary {
                self.fetchedJson = json
            }
            else {
                print("JSON object is nil")
            }
        }
        catch let error as NSError {
            print("Error converting data to Array: \(error.localizedDescription)")
        }
    }
    
    func completeOperation() {
        isExecuting = false
        isFinished = true
    }
    
    private var override_executing : Bool
    override var isExecuting : Bool {
        get { return override_executing }
        set {
            willChangeValue(forKey: "isExecuting")
            override_executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var override_finished : Bool
    override var isFinished : Bool {
        get { return override_finished }
        set {
            willChangeValue(forKey: "isFinished")
            override_finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }
}
