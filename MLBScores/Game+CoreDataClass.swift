//
//  Game+CoreDataClass.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import Foundation
import CoreData


public class Game: NSManagedObject {

    func gameListStatus() -> String {
        
        if let status = status {
            switch  status {
            case GameStatus.Preview, GameStatus.Pregame, GameStatus.Warmups:
                return time! + time_zone! + " @ " + venue!
            case GameStatus.Final, GameStatus.GameOver:
                return GameStatus.Final
            case GameStatus.InProgress:
                if let inningState = inningState {
                    return inningState + " of the " + String(inning) + postfixForInning()
                }
                else {
                    return status
                }
            default:
                return status
            }
        }
        else {
            return ""
        }
    }
    
    func postfixForInning() -> String {
        if inning == 1 {
            return "st"
        }
        else if inning == 2 {
            return "nd"
        }
        else if inning == 3 {
            return "rd"
        }
        else {
            return "th"
        }
    }
}
