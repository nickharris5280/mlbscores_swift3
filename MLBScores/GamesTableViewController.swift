//
//  GamesTableViewController.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit
import CoreData

class GamesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var fetchedResultsController: NSFetchedResultsController<Game>?
    
    var persistentContainer: NSPersistentContainer? {
        didSet {
            if isViewLoaded {
                configureFetchedResultsController()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if persistentContainer != nil {
            configureFetchedResultsController()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if let fetchedResultsController = fetchedResultsController {
            print(fetchedResultsController.sections!.count)
            return 1;//fetchedResultsController.sections!.count
        }
        else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let fetchedResultsController = fetchedResultsController,
              let sections = fetchedResultsController.sections else {
                return 0;
        }
        
        return sections[section].numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> GameTableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell") as? GameTableViewCell,
              let game = fetchedResultsController?.object(at: indexPath) else {
            fatalError("Unable to dequeue a cell")
        }
        
        cell.configureCell(game: game)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    
    // MARK: - Fetched Results Controller
    
    func configureFetchedResultsController() {
        
        guard let persistentContainer = persistentContainer else {
            return;
        }
        
        let fetchRequest: NSFetchRequest<Game> = Game.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "time", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self
        
        do {
            try fetchedResultsController?.performFetch()
        }
        catch let error as NSError {
            print("Error converting data to Array: \(error.localizedDescription)")
        }
        
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: AnyObject, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else {
                fatalError("GamesTableViewController: nil indexpath")
            }
            tableView.insertRows(at: [newIndexPath as IndexPath], with: .fade)
            
        case .update:
            guard let indexPath = indexPath,
                let game = anObject as? Game else {
                    fatalError("GamesTableViewController: not enough data to update a cell")
            }
            let gameCell = self.tableView(self.tableView, cellForRowAt: indexPath)
            gameCell.configureCell(game: game)
            
        case .delete:
            guard let indexPath = indexPath else {
                fatalError("GamesTableViewController: nil indexpath")
            }
            tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            
        case .move:
            guard let newIndexPath = newIndexPath,
                let indexPath = indexPath else {
                    fatalError("GamesTableViewController: not enough data to move a cell")
            }
            tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            tableView.insertRows(at: [newIndexPath as IndexPath], with: .fade)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
