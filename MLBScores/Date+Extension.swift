//
//  Date+Extension.swift
//  MLBScores
//
//  Created by Nick Harris on 8/24/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import Foundation

extension Date {
    
    static func todaysDayOfYear() -> Int16 {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.ordinality(of: .day, in: .year, for: date)
        
        return Int16(day!)
    }
}
