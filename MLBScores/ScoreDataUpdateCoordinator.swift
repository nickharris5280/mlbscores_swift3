//
//  ScoreDataUpdateCoordinator.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import Foundation
import CoreData

class ScoreDataUpdateCoordinator {
    
    weak var persistentContainer: NSPersistentContainer!
    
    let operationQueue = OperationQueue()
    
    init(persistentContainer: NSPersistentContainer) {
        
        self.persistentContainer = persistentContainer
    }
    
    func updateScoreData() {
        
        let feedUrlString = createFeedUrlString()
        
        // create concrete operations
        let fetchFeedOperation = FetchJsonOperation(urlString: feedUrlString)
        let parseMLBScoreDataOperation = ParseMLBScoreDataOperation()
        let updateCoreDataOperaton = UpdateCoreDataOperation(persistentContainer: persistentContainer)
        
        // create transfer block operations
        let transferScoreDataOperation = BlockOperation { [unowned fetchFeedOperation, unowned parseMLBScoreDataOperation] in
            parseMLBScoreDataOperation.gameJsonDictionary = fetchFeedOperation.fetchedJson
        }
        
        let transferParsedDataOperation = BlockOperation { [unowned parseMLBScoreDataOperation, unowned updateCoreDataOperaton] in
            updateCoreDataOperaton.allGameData = parseMLBScoreDataOperation.parsedGameData
        }
        
        // create
        transferScoreDataOperation.addDependency(fetchFeedOperation)
        parseMLBScoreDataOperation.addDependency(transferScoreDataOperation)
        transferParsedDataOperation.addDependency(parseMLBScoreDataOperation)
        updateCoreDataOperaton.addDependency(transferParsedDataOperation)
        
        operationQueue.addOperation(fetchFeedOperation)
        operationQueue.addOperation(transferScoreDataOperation)
        operationQueue.addOperation(parseMLBScoreDataOperation)
        operationQueue.addOperation(transferParsedDataOperation)
        operationQueue.addOperation(updateCoreDataOperaton)
    }
    
    func createFeedUrlString() -> String {
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day , .month , .year], from: date)
        
        guard let year = components.year,
            let month = components.month,
            let day = components.day else {
                fatalError("Could not get date components to create the feed URL string")
        }
        
        let monthString = (month < 10) ? "0\(month)" : String(month)
        let dayString = (day < 10) ? "0\(day)" : String(day)
        
        let feedUrlString = "http://gd2.mlb.com/components/game/mlb/year_\(year)/month_\(monthString)/day_\(dayString)/master_scoreboard.json"
        
        return feedUrlString
    }
}
