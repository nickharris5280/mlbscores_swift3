//
//  GameData.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import Foundation

struct GameData {
    
    let awayTeam: String
    let awayTeamScore: Int16
    let homeTeam: String
    let homeTeamScore: Int16
    let identifier: String
    let inning: Int16
    let inningState: String?
    let status: String
    let time: String
    let time_zone: String
    let venue: String
    let dayOfYear: Int16
    let statusGroup: GameStatusGroup

}

struct GameStatus {
    
    static let Preview = "Preview"
    static let GameOver = "Game Over"
    static let Final = "Final"
    static let Pregame = "Pre-Game"
    static let Warmups = "Warmups"
    static let Delayed = "Delayed"
    static let InProgress = "In Progress"
}

enum GameStatusGroup: Int16 {
    
    case Pregame = 0
    case InProgress = 1
    case Postgame = 2
    case Unknown = 3
}
