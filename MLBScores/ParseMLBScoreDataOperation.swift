//
//  ParseMLBScoreDataOperation.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit

class ParseMLBScoreDataOperation: Operation {
    
    struct JSONKeys {
        static let awayTeamCity = "away_team_city"
        static let awayTeamName = "away_team_name"
        static let homeTeamCity = "home_team_city"
        static let homeTeamName = "home_team_name"
        static let linescore = "linescore"
        static let runs = "r"
        static let away = "away"
        static let home = "home"
        static let venue = "venue"
        static let gameIdentifier = "id"
        static let status = "status"
        static let inning = "inning"
        static let inningState = "inning_state"
        static let time = "time"
        static let timeZone = "time_zone"
    }
    
    var gameJsonDictionary: NSDictionary?
    var parsedGameData: [GameData]
    
    override init() {
        parsedGameData = []
    }
    
    override func main() {
        
        guard let gameJsonDictionary = gameJsonDictionary else {
            fatalError("Game Json Dictionary was not set")
        }
        
        let data = gameJsonDictionary["data"] as! NSDictionary
        let gamesDictionary = data["games"] as! NSDictionary
        let allGames = gamesDictionary["game"] as! NSArray

        for gameInfo in allGames {
            
            guard let gameInfoDictionary = gameInfo as? NSDictionary else {
                fatalError("Error parsing game info: Game Info was not a dictionary")
            }
            let gameData = parseGameData(gameInfoDictionary: gameInfoDictionary)
            parsedGameData.append(gameData)
        }
    }

    func parseGameData(gameInfoDictionary: NSDictionary) -> GameData {
        
        let awayTeamName = parseTeamName(gameInfoDictionary: gameInfoDictionary, teamCityJSONKey: JSONKeys.awayTeamCity, teamNameJSONKey: JSONKeys.awayTeamName)
        let homeTeamName = parseTeamName(gameInfoDictionary: gameInfoDictionary, teamCityJSONKey: JSONKeys.homeTeamCity, teamNameJSONKey: JSONKeys.homeTeamName)
        
        let score = parseScore(gameInfoDictionary: gameInfoDictionary)
        let status = parseStatus(gameInfoDictionary: gameInfoDictionary)
        
        let time = gameInfoDictionary[JSONKeys.time] as! String
        let timeZone = gameInfoDictionary[JSONKeys.timeZone] as! String
        let venue = gameInfoDictionary[JSONKeys.venue] as! String
        let identifier = gameInfoDictionary[JSONKeys.gameIdentifier] as! String
        
        return GameData(awayTeam: awayTeamName,
                        awayTeamScore: score.awayScore,
                        homeTeam: homeTeamName,
                        homeTeamScore: score.homeScore,
                        identifier: identifier,
                        inning: status.inning,
                        inningState: status.inningState,
                        status: status.status,
                        time: time,
                        time_zone: timeZone,
                        venue: venue,
                        dayOfYear: Date.todaysDayOfYear(),
                        statusGroup: statusGroup(status: status.status))
    }
    
    func parseTeamName(gameInfoDictionary: NSDictionary, teamCityJSONKey: String, teamNameJSONKey: String) -> String {
        
        let teamCity = gameInfoDictionary[teamCityJSONKey] as! String
        let teamName = gameInfoDictionary[teamNameJSONKey] as! String
        
        let fullTeamName: String
        if teamCity.contains(teamName) {
            fullTeamName = teamCity
        }
        else {
            fullTeamName = "\(teamCity) \(teamName)"
        }
        
        return fullTeamName
    }
    
    func parseScore(gameInfoDictionary: NSDictionary) -> (awayScore: Int16, homeScore: Int16) {
        
        guard let liveScore = gameInfoDictionary[JSONKeys.linescore] as? NSDictionary else {
            return (awayScore: 0, homeScore: 0)
        }
        
        let runs = liveScore[JSONKeys.runs] as! NSDictionary
        let awayScoreString = runs[JSONKeys.away] as! String
        let homeScoreString = runs[JSONKeys.home] as! String
        
        let awayScoreInt = Int16(awayScoreString)!
        let homeScoreInt = Int16(homeScoreString)!
        
        return (awayScore: awayScoreInt, homeScore: homeScoreInt)
    }
    
    func parseStatus(gameInfoDictionary: NSDictionary) -> (inning: Int16, inningState: String?, status: String) {

        let statusDictionary = gameInfoDictionary[JSONKeys.status] as! NSDictionary
        
        let inningString = statusDictionary[JSONKeys.inning] as? String
        let inningState = statusDictionary[JSONKeys.inningState] as? String
        let status = statusDictionary[JSONKeys.status] as! String
        
        
        let inningInt: Int16
        if let inningString = inningString {
           inningInt = Int16(inningString)!
        }
        else {
            inningInt = 0
        }
        
        return (inning: inningInt, inningState: inningState, status: status)
    }
    
    func statusGroup(status: String) -> GameStatusGroup {
        
        switch status {
        case GameStatus.Preview, GameStatus.Warmups, GameStatus.Pregame: return GameStatusGroup.Pregame
        case GameStatus.Delayed, GameStatus.InProgress: return GameStatusGroup.InProgress
        case GameStatus.Final, GameStatus.GameOver: return GameStatusGroup.Postgame
        default: return GameStatusGroup.Unknown
        }
    }
}
