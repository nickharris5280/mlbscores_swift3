//
//  AppDelegate.swift
//  MLBScores
//
//  Created by Nick Harris on 8/20/16.
//  Copyright © 2016 Clifton Garage Mobile LLC. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var persistentContainer: NSPersistentContainer?
    var scoreDataUpdateCoordinator: ScoreDataUpdateCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
       
        guard let rootNavigationController = window?.rootViewController as? UINavigationController,
            let gamesTableViewController = rootNavigationController.viewControllers[0] as? GamesTableViewController else {
                return false
        }
        
        persistentContainer = NSPersistentContainer(name: "MLBScores")
        persistentContainer!.loadPersistentStores(completionHandler: { [unowned self] (storeDescription, error) in
            if let error = error as NSError? {

                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            else {
                self.scoreDataUpdateCoordinator = ScoreDataUpdateCoordinator(persistentContainer: self.persistentContainer!)
                self.scoreDataUpdateCoordinator?.updateScoreData()
                
                gamesTableViewController.persistentContainer = self.persistentContainer
                self.persistentContainer?.viewContext.automaticallyMergesChangesFromParent = true
            }
        })

        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        do {
            print("Save Core Data")
            try self.persistentContainer?.viewContext.save()
        }
        catch let error as NSError {
            print("Error saving CoreData: \(error.localizedDescription)")
        }
    }
}

